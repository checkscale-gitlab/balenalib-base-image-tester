
all:	remove .gitlab-ci.yml

.gitlab-ci.yml:
	python generator.py > $@

.PHONY: remove

remove:
	rm .gitlab-ci.yml
