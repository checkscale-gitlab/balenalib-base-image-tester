#!/usr/bin/env bash

set -o xtrace -o errexit -o pipefail

# shellcheck disable=SC2034
OS=$1
SOFTWARE_VERSION=$2
SOFTWARE_VARIANT=$3

function check_vesion {
    local queried_dotnet_version

    queried_dotnet_version=$(dotnet --info 2>&1 | grep -m 1 Version: | awk '{ print $2 }')
    [[ "${queried_dotnet_version}" =~ ^"${SOFTWARE_VERSION}".* ]] || ( echo ".NET version doesn't match." ; exit 1 )
}

function dotnet_run {
    local result

    if [ "$SOFTWARE_VARIANT" = "sdk" ]; then
        if [ -z "$(dotnet --list-sdks)" ] ; then
             echo ".NET SDK should be installed, and isn't." ; exit 1
        fi
        dotnet new console -o testApp
        cd testApp
        result=$(dotnet run)
        [ "$result" == "Hello World!" ] || ( echo ".NET run failed." ; exit 1 )
    else
        echo "No .NET compilation test."
    fi
}

function main {
    check_vesion
    dotnet_run
}

main