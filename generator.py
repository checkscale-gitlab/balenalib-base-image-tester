#Import necessary functions from Jinja2 module
from jinja2 import Environment, FileSystemLoader
import yaml
import re
import itertools

def generate_jobs(config):
    '''Generate the required job list and tags from the combination of input settings
    '''
    jobs = {}
    if config['variants'] is None:
        config['variants'] = ['']
    res = itertools.product(config['devices'], config['distros'], config['languages'], config['variants'])
    for r in res:
        device, distro, language, variant = r
        thisconfig = {'devices': device, 'distros': distro, 'languages': language, 'variants': variant}
        distro_versions = ['']
        try:
            distro_versions += config['distros'][distro]['versions']
        except:
            pass
        language_versions = config['languages'][language]['versions'] if config['languages'][language] else ['']
        try:
            language_variants = config['languages'][language]['variants']
        except KeyError:
            language_variants = ['']

        res2 = itertools.product(distro_versions, language_versions, language_variants)
        for r2 in res2:
            distro_version, language_version, language_variant = r2
            # Process 'exclude' and 'only' tags
            try:
                excludeThis, hasOnly, foundOnly = False, False, False
                combos = itertools.product([device, distro, language, variant, distro_version],
                                               ['devices', 'distros', 'languages', 'variants'])
                for comboitem, comboconfig in combos:
                    try:
                        excludeRules = config[comboconfig][thisconfig[comboconfig]]['exclude']
                        excludeThese = []
                        for excludeRule in excludeRules:
                            excludeMatch = re.match("^(?P<exclude>[^|\s]*)\|?(?P<criteria>[^|\s]*)", excludeRule)
                            if excludeMatch:
                                if excludeMatch.group('criteria') == '':
                                    excludeThese += [ excludeMatch.group('exclude') ]
                                elif excludeMatch.group('criteria') in [distro_version, language_version, language_variant]:
                                    excludeThese += [ excludeMatch.group('exclude') ]
                        if comboitem in excludeThese:
                            excludeThis = True
                            # Exclude takes precedent over only
                            break
                    except:
                        pass
                    try:
                        if config[comboconfig][thisconfig[comboconfig]]['only']:
                            hasOnly = True
                            if comboitem in config[comboconfig][thisconfig[comboconfig]]['only']:
                                foundOnly = True
                    except:
                        pass
                if excludeThis or (hasOnly and not foundOnly):
                    continue
            except:
                pass

            jobname = ''
            jobflags = ''

            jobname += f'{device}'
            jobflags += f' -d "{device}"'

            jobname += f'-{distro}' if distro != 'debian' else ''
            jobflags += f' -o "{distro}"'

            jobname += f'-{language}'
            jobflags += f' -l "{language}"'

            jobname += ':'
            jobname += f'{language_version}'
            jobflags += f' -s "{language_version}"'

            if language_variant != '':
                jobname += f'-{language_variant}'
                jobflags += f' -x "{language_variant}"'

            jobname += f'-{distro_version}' if distro_version != "" else ''
            jobflags += f' -r "{distro_version}"' if distro_version != "" else ''

            jobname += f'-{variant}' if variant != "" else ''
            jobflags += f' -v "{variant}"' if variant != "" else ''

            jobtags = config['devices'][device]['tag'] if config['devices'][device] else [device]

            ff_helper = True if device == 'i386' else False

            jobs[jobname] = {'tags': jobtags, 'flags': jobflags, 'ff_helper': ff_helper }
    return { 'images': jobs, 'repo': config['repo'] }

def generate_template(templatefile, jobs):
    env = Environment(loader = FileSystemLoader('./'), trim_blocks=True, lstrip_blocks=True)
    template = env.get_template(templatefile)
    return template.render(jobs)

if __name__ == "__main__":
    config = yaml.load(open('config.yml'), Loader=yaml.SafeLoader)
    print(generate_template("gitlab-ci.template", generate_jobs(config)))