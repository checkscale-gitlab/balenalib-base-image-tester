#!/usr/bin/env bash

set -o xtrace -o errexit -o pipefail

# shellcheck disable=SC2034
OS=$1
SOFTWARE_VERSION=$2

function check_vesion {
    local queried_node_version
    local queried_yarn_version

    [ -n "$NODE_VERSION" ] || ( echo "No NODE_VERSION env var!" ; exit 1 )
    queried_node_version=$(node --version)
    [ "${queried_node_version}" == "v${NODE_VERSION}" ] || ( echo "Node version doesn't match." ; exit 1 )

    [[ "${queried_node_version}" =~ ^v"${SOFTWARE_VERSION}".* ]] || ( echo "Node version and tag doesn't match." ; exit 1)

    [ -n "$YARN_VERSION" ] || ( echo "No YARN_VERSION env var!" ; exit 1 )
    queried_yarn_version=$(yarn --version)
    [ "${queried_yarn_version}" == "${YARN_VERSION}" ] || ( echo "Yarn version doesn't match." ; exit 1)
}

function npm_install {
    local result

    npm install request

    result=$(node -e "let request = require('request'); console.log('check')")
    [ "$result" == "check" ] || ( echo "Node install & run failed." ; exit 1)
}

function main {
    check_vesion
    npm_install
}

main